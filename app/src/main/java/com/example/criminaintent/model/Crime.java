package com.example.criminaintent.model;

import java.util.Date;
import java.util.UUID;

public class Crime {

    private UUID mId;
    private String mTiile;
    private Date mDate;
    private boolean mSolved;
    private String mSuspect;

    public void setId(UUID id) {
        mId = id;
    }

    public String getSuspect() {
        return mSuspect;
    }

    public void setSuspect(String suspect) {
        mSuspect = suspect;
    }

    public Crime() {
        this(UUID.randomUUID());
    }

    public Crime(UUID id){
        mId = id;
        mDate = new Date();
    }

    public UUID getId() {
        return mId;
    }

    public String getTiile() {
        return mTiile;
    }

    public void setTiile(String tiile) {
        mTiile = tiile;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public boolean isSolved() {
        return mSolved;
    }

    public void setSolved(boolean solved) {
        mSolved = solved;
    }

    public String getPhotoFileName(){
        return "IMG_"+getId().toString()+".jpg";
    }
}
